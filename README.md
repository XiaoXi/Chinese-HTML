# Chinese HTML

⭐ 一款全中文 JavaScript 解释器超文本标记语言 ⭐

## 🤔 这是什么

随着国内前端程序员越来越多，学习英文的网页语言渐渐地成为了负担。为了顺应中文编程的需求，现在已经有不少成型的中文脚本语言，但中文的超文本标记语言还没有见到，这才有了**「博大精深」**语言！

在线预览：[https://chinese-html.soraharu.com/](https://chinese-html.soraharu.com/)

![二维码](./images/二维码.png)

## 🍭 使用说明

「博大精深」语言上手极其简单，引用 `博大精深.js` 并执行 `博大精深.变身(document.body)` 即完成了「博大精深」的启动和转义！

1. 使用 `【】` 符号包裹标签名。
2. 样式表中同样可以中文表达式书写。
3. 使用 `：` 符号赋值属性。
4. 网页源代码里全是中文了你还要啥？

> 备注一：避免了 在手写中文正文网页代码时不断切换输入法状态 来输入 `<` 和 `>`。

> 备注二：标题上 你可以使用「大标题」、「标题一」等多个意义相同的中文标签名，但要注意尽量避开【】这两个符号的使用，使用括号时推荐使用更加能体现中文博大精深的 `「` 以及 `」` 符号。

> 备注三：配合「博大精深」指定样式表时，「博大精深」会把中文标签改为标签相应的 **类** 名，所以还是需要在表达式中使用类表达式。

> 备注四：通过 `【、】`、`【。】` 标示符，可「闭合前一个打开的标签」。

> 备注五：有什么不懂的其实直接看下示例网页的源代码就好啦~

## 📜 开源许可

基于 [MIT License](https://choosealicense.com/licenses/mit/) 许可进行开源。

## 💕 引用和感谢

1. [Standard Generalized Markup Language - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/Standard_Generalized_Markup_Language#SHORTTAG)
2. [SHORTTAGS – THE ODD SIDE OF HTML 4.01](https://www.w3.org/blog/2007/10/shorttags/)
3. [Appendix B: Performance, Implementation, and Design Notes](https://www.w3.org/TR/html401/appendix/notes.html#h-B.3.7)
